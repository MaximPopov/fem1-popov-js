const createCircleBtn = document.getElementById('createCircleBtn');

const inputDiameter = document.createElement('input');
inputDiameter.type = 'text';
inputDiameter.className = 'default-input';
inputDiameter.placeholder = 'Диаметр круга';

const drawCirclesBtn = document.createElement('button');
drawCirclesBtn.className = 'default-btn';
drawCirclesBtn.innerText = 'Нарисовать';

const circlesWrapper = document.createElement('div');
circlesWrapper.className = 'circlesWrapper';

function setDiameter() {
    document.body.appendChild(inputDiameter);
    document.body.appendChild(drawCirclesBtn);
}

function randomColor() {
    return `rgb(${Math.random()*255}, ${Math.random()*255}, ${Math.random()*255})`
}

function drawCircles() {
    for (let i = 0; i<100; i++) {
        const circle = document.createElement('div');
        circle.style.width = inputDiameter.value;
        circle.style.height = inputDiameter.value;
        circle.style.borderRadius = inputDiameter.value;
        circle.style.backgroundColor = randomColor();
        circle.className = 'circle';
        circlesWrapper.appendChild(circle);
    }
    circlesWrapper.style.gridTemplateColumns = `repeat(10, ${inputDiameter.value})`;
    document.body.appendChild(circlesWrapper);
}

document.body.onclick = (event) => {
    let target = event.target;
    if (target === createCircleBtn) {
        setDiameter();
    } else if (target === drawCirclesBtn) {
        drawCircles();
    } else if (target.className === 'circle') {
       target.remove();
    }
};