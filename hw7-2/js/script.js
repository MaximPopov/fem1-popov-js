
function createList(arr) {
    let ul = buildList(arr);
    ul.id = 'list';
    document.body.appendChild(ul);
}

function buildList(arr) {
    let ul = document.createElement('ul');
    arr.forEach(function(elem) {
        if (typeof(elem) === 'object') {
            ul.appendChild(buildList(elem));
        } else {
            let li = document.createElement('li');
            li.innerHTML = `${elem}`;
            ul.appendChild(li);
        }
    });
    return ul
}

function countdown() {
    timerValue.innerText = timerValue.innerText - 1;
    if (+timerValue.innerText === 0) {
        removeList();
        removeTimerValue();
        clearInterval(timer);
    }
}

function removeList() {
    document.body.removeChild(document.getElementById('list'));
}

function removeTimerValue() {
    document.body.removeChild(document.getElementById('timerValue'));
}

/*  Уставовка начального значения обратного отсчета */
timerValue.innerText = 10;

/*  Запуск таймера обратного отсчета */
const timer = setInterval(countdown, 1000);

/*  Вызов функции вывода списка на страницу */
createList(['hello', 'world', 'Kiev', ['Sadovaya str.', 'Centralnaya str.', ['2', '4', '6']], 'Odessa', 'Lviv']);
