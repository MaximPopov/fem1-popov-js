const changeThemeBtn = document.createElement('button');
changeThemeBtn.innerHTML = 'change theme';
changeThemeBtn.className = 'change-theme-bnt';
document.querySelector('.navbar-top-menu').appendChild(changeThemeBtn);

const setDarkTheme = () => {
    document.querySelector('.navbar-top').style.backgroundColor = '#000';
    document.querySelectorAll('.basic-block').forEach((elem) => {
        elem.className = 'basic-block basic-block-dark'
    });
    document.querySelector('.parallax').style.backgroundColor = '#000';
    document.querySelector('.logo-text').style.color = '#acbdc7';
};

const setWhiteTheme = () => {
    document.querySelector('.navbar-top').style.backgroundColor = '#fff';
    document.querySelectorAll('.basic-block').forEach((elem) => {
        elem.className = 'basic-block'
    });
    document.querySelector('.parallax').style.backgroundColor = '#fff';
    document.querySelector('.logo-text').style.color = '#424f57';
};

changeThemeBtn.onclick = () => {
    switch (localStorage.getItem('theme')) {
        case 'white':
            localStorage.setItem('theme', 'dark');
            setDarkTheme();
            break;
        case 'dark':
            localStorage.setItem('theme', 'white');
            setWhiteTheme();
            break;
    }
};

/*
Перемешение по странице при нажатие на элементы меню
*/
const scrollByClick = (btn, y) => {
    document.getElementById(btn).onclick = () => {
        document.querySelector('.parallax').scroll(0, y)
    }
};
scrollByClick('btnWhat', 700);
scrollByClick('btnCompanies', 1130);
scrollByClick('btnMeet', 1780);
scrollByClick('btnTestimonials', 2500);
scrollByClick('btnWhy', 2800);

/*
Отображение страницы после полной загрузки DOM
*/
document.addEventListener("DOMContentLoaded", () => {
    if (localStorage.getItem('theme') === null) {
        localStorage.setItem('theme', 'white');
    } else if (localStorage.getItem('theme') === 'dark') {
        setDarkTheme();
    }
    setTimeout( () => {
        document.querySelector('.parallax').style.height = '100vh';
    }, 100)
});

