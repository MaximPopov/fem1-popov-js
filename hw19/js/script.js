function toCloneObject(object) {
    let clonedObject = {
    };
    for (let key in object) {
        if (typeof(object[key]) === "object") {
            clonedObject[key] = toCloneObject(object[key]);
        }
        clonedObject[key] = object[key];
    }
    return clonedObject
}


/*      Создаем тестовый объект     */

let someObject = {
    key1: 133,
    key2: 'some value',
    key3: [13, 123, 'some str', {
        level3Key1: 133,
        level3Key2: 'some value on level 3',
    }]
};


/*      Создаем клон тестового обьекта    */

let clonedObject = toCloneObject(someObject);