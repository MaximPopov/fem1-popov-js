function createNewUser() {
    let newUser = {
        firstName: prompt('Enter first name', 'Maksim'),
        lastName: prompt('Enter last name', 'Popov'),
        birthday: prompt('Enter your birthday (dd.mm.yyyy) ', '11.03.1990'),
        getLogin: function () {
            return this.firstName.charAt(0).toLowerCase() + this.lastName.toLowerCase()
        },
        getAge: function () {
            let date = new Date();
            if (this.birthday.slice(-7,-5) >= date.getMonth() && this.birthday.slice(-10,-8) > date.getDate()) {
                return date.getFullYear() - this.birthday.slice(-4) - 1
            } else {
                return date.getFullYear() - this.birthday.slice(-4)
            }
        },
        getPassword: function () {
            return this.firstName.charAt(0).toUpperCase() + this.lastName.toLowerCase() + this.birthday.slice(-4)
        }
    };
    return newUser
}

let user = createNewUser();
console.log(`User: ${user.getLogin()}\nAge: ${user.getAge()}\nPass: ${user.getPassword()}`);