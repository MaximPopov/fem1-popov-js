let table = document.createElement('table');
for (let i = 0; i < 30; i++) {
    let tr = document.createElement('tr');
    for (let i = 0; i < 30; i++) {
        let td = document.createElement('td');
        td.classList.add('light');
        tr.appendChild(td)
    }
    table.appendChild(tr)
}
document.body.appendChild(table);

document.body.onclick = (event) => {
    if (event.target.tagName === 'TD') {
        let elem = event.target;
        elem.classList.toggle('active')
    } else if (event.target === event.currentTarget) {
        table.classList.toggle('invert')
    }
};