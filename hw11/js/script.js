const btnsArr = document.querySelector('.btn-wrapper').children;

document.body.onkeydown = (event) => {
    for (let i = 0; i < btnsArr.length; i++) {
        if (btnsArr[i].innerText.toUpperCase() === event.key.toUpperCase()) {
            btnsArr[i].style.backgroundColor = 'blue';
        } else {
            btnsArr[i].style.backgroundColor = '#33333a';
        }
    }
};