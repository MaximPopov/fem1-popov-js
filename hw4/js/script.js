function createNewUser() {
    let newUser = {
        firstName: prompt('Enter first name', 'Maksim'),
        lastName: prompt('Enter last name', 'Popov'),
        getLogin: function () {
            return this.firstName.charAt(0).toLowerCase() + this.lastName.toLowerCase()
        }
    };
    return newUser
}

let user = createNewUser();
console.log(`Login: ${user.getLogin()}`);