$('.tabs-content').children().hide();
$('.tabs-content').children().eq(0).show();

$('.tabs').on('click', (event) => {
    $('.tabs-title').each((index, element) => {
        if (element === event.target) {
            $('.tabs-content').children().eq(index).show();
            $(element).addClass('active');
        } else {
            $('.tabs-content').children().eq(index).hide();
            $(element).removeClass('active');
        }
    })
});