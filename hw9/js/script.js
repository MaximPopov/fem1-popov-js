const tabs = document.getElementById('tabs');
const tabsContent = document.getElementById('tabsContent');

for (let i = 1; i<tabsContent.children.length; i++) {
    tabsContent.children[i].style.display = 'none';
}

for (let i = 0; i<tabs.children.length; i++) {
    tabs.children[i].dataset.index = `${i}`;
}

tabs.onclick = () => {
    let clickedLi = event.target;
    for (let i = 0; i<tabsContent.children.length; i++) {
        tabsContent.children[i].style.display = 'none';
        tabs.children[i].className = 'tabs-title';
    }
    tabsContent.children[clickedLi.dataset.index].style.display = 'block';
    clickedLi.className += ' active';
};