let index = 0;

const imagesList = document.querySelectorAll('.image-to-show');

const changeSlide = () => {
    imagesList.forEach((elem)=>{
        elem.style.display = 'none'
    });
    imagesList[index].style.display = 'block';
    index++;
    if (index === imagesList.length) {
        index = 0;
    }
};

changeSlide();

let timerId =  setInterval(changeSlide, 10000);

const stopButton = document.createElement('button');
stopButton.innerHTML = "Прекратить";
stopButton.className = 'btn';
document.body.appendChild(stopButton);
stopButton.onclick = () => {
    clearInterval(timerId);
};

const goButton = document.createElement('button');
goButton.innerHTML = "Возобновить";
goButton.className = 'btn';
document.body.appendChild(goButton);
goButton.onclick = () => {
    timerId =  setInterval(changeSlide, 10000);
};
