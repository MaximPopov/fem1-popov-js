function filterBy(arr, type) {
    return arr.filter(val => typeof (val) !== type)
}

console.log(filterBy(['hello', 'world', 23, '23', null], 'string'));