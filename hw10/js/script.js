const passInputFirst = document.getElementById('passInputFirst');
const passInputSecond = document.getElementById('passInputSecond');
const passIconFirst = document.getElementById('passIconFirst');
const passIconSecond = document.getElementById('passIconSecond');
const submitBtn = document.getElementById('submitBtn');
const errorMassage = document.createElement('span');
errorMassage.className = 'error-massage';
errorMassage.innerText = 'Нужно ввести одинаковые значения!';

function showPass(input, icon) {
    if (input.type === 'password') {
        input.type = 'text';
        icon.className = 'fas fa-eye-slash icon-password';
    } else {
        input.type = 'password';
        icon.className = 'fas fa-eye icon-password';
    }
}

passIconFirst.onclick = () => {
    showPass(passInputFirst, passIconFirst)
};

passIconSecond.onclick = () => {
    showPass(passInputSecond, passIconSecond)
};

submitBtn.onclick = () => {
    if (passInputFirst.value === passInputSecond.value) {
        errorMassage.remove();
        alert('You are welcome')
    } else {
        document.querySelector('.password-form').insertBefore(errorMassage, submitBtn)
    }
};
