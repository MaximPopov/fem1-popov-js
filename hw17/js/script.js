function fibonacci (F0, F1, n) {
    if (n > 0) {
        return fibonacci(F1, F0 + F1, n - 1);
    } else if (n < 0) {
        return fibonacci(F1 - F0, F0, n + 1);
    } else  return F0;
}

let F0 = Number(prompt('Enter first number', '0'));
let F1 = Number(prompt('Enter second number', '1'));
let n = Number(prompt('Enter index number', '10'));

while (isNaN(F0) || isNaN(F1) || isNaN(n)) {
    alert('Numbers is not correct! Enter numbers again, please.');
    F0 = Number(prompt('Enter first number', '0'));
    F1 = Number(prompt('Enter second number', '1'));
    n = Number(prompt('Enter index number', '10'));
}

alert(n + '-th number of Fibonacci is:' + fibonacci(F0, F1, n));