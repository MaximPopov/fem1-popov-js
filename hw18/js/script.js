let student = {
    name: prompt('Enter your name', 'Maksim'),
    lastName: prompt('Enter your last name', 'Popov'),
    tabel: {},
    fillTabel: () => {
        let course = prompt('Enter course', 'Biology');
        while (course) {
            student.tabel[course] = +prompt('Enter mark', '10');
            course = prompt('Enter course', 'Biology');
        }
    },
    getEducationProgress: () => {
        let numberOfBedMarks = 0;
        for (let key in student.tabel) {
            if (student.tabel[key] < 4) {
                numberOfBedMarks++;
            }
        }
        if (numberOfBedMarks === 0) {
            alert('Плохих оценок нет. Студент переведен на следующий курс')
        } else {
            alert(`Количество плохих оценок: ${numberOfBedMarks}`)
        }
    },
    getGrantStatus: () => {
        let sumOfMarks = 0;
        let numberOfCourses = 0;
        for (let key in student.tabel) {
            sumOfMarks += student.tabel[key];
            numberOfCourses++;
        }
        if (sumOfMarks/numberOfCourses > 7) {
            alert('Студенту назначена стипендия')
        } else {
            alert('Степендия не назначена!')
        }
    }
};

student.fillTabel();
student.getEducationProgress();
student.getGrantStatus();
