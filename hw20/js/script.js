function timeToIt(team, tasks, deadline) {
    const date = new Date();
    const necessaryDays = tasks.reduce((sum, val) => sum + val) / team.reduce((sum, val) => sum + val);
    let availableDays = 0;
    while (date.getFullYear() !== deadline.getFullYear() || date.getMonth() !== deadline.getMonth() || date.getDate() !== deadline.getDate()) {
        if (date.getDay() !== 0 && date.getDay() !== 6) {
            availableDays++;
        }
        date.setDate(date.getDate() + 1);
    }
    if (availableDays >= necessaryDays) {
        alert(`Все задачи будут успешно выполнены за ${availableDays-necessaryDays} дней до наступления дедлайна!`)
    } else {
        alert(`Команде разработчиков придется потратить дополнительно ${Math.ceil((necessaryDays - availableDays) * 8)} часов после дедлайна, чтобы выполнить все задачи в беклоге`)
    }
}

/*   Тестовые данные    */
const team = [6, 8, 4, 9];
const tasks = [100, 100, 80, 100, 80, 80];
const deadline = new Date('06.24.2019');

timeToIt(team, tasks, deadline);