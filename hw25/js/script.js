const leftArrow = document.getElementById('leftArrow');
const rightArrow = document.getElementById( 'rightArrow');
const itemsList = document.querySelectorAll('.slider-item');
let position = 0;

itemsList.forEach((item) => {
    item.style.left = `${position}%`;
    position += 100;
    if (position === itemsList.length * 100 - 200) {
        position = -200
    }
});

const leftArrowHandler = () => {
    itemsList.forEach((item) => {
        setTimeout(() => {
            position = +item.style.left.slice(0, -1) + 100;
            if (position === itemsList.length * 100 - 200) {
                item.style.opacity = '0';
                position = -200;
            } else {
                item.style.opacity = '1';
            }
            item.style.left = `${position}%`;
        },0)
    });

};

leftArrow.onclick = () => {
    leftArrowHandler();
    leftArrow.style.pointerEvents = 'none';
    setTimeout(() => {
        leftArrow.style.pointerEvents = 'auto';
    }, 400)
};

const rightArrowHandler = () => {
    itemsList.forEach((item) => {
        position = +item.style.left.slice(0, -1) - 100;
        if (position === -300) {
            item.style.opacity = '0';
            position = itemsList.length * 100 - 300;
        } else {
            item.style.opacity = '1';
        }
        item.style.left = `${position}%`;
    });
};

rightArrow.onclick = () => {
    rightArrowHandler();
    rightArrow.style.pointerEvents = 'none';
    setTimeout(() => {
        rightArrow.style.pointerEvents = 'auto';
    }, 400)
};