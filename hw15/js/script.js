$('.top-navbar-menu').on('click', (event) => {
        const id = $(event.target).attr('href');
        const top = $(id).offset().top;
        $('html').animate({scrollTop: top}, 1500);
        return false
});

$('.up-btn').on('click', () => {
    $('html').animate({scrollTop: '0'}, 1500);
    return false
});

$(window).scroll( () => {
    if ($('html').scrollTop() > window.innerHeight) {
        $('.up-btn').show();
    } else {
        $('.up-btn').hide();
    }
});

$('.top-rated-bnt').on('click', () => {
    if ($('.top-rated-bnt').text() === 'Show') {
        $('.top-rated-container').animate({height: $('.top-rated-items').eq(0).css('height')}, 1000);
        $('.top-rated-bnt').text('Hide');
    } else {
        $('.top-rated-container').animate({height: '0'}, 1000);
        $('.top-rated-bnt').text('Show');
    }
});