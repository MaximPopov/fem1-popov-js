function createNewUser() {
    let newUser = {
        getLogin: function () {
            return this.firstName.charAt(0).toLowerCase() + this.lastName.toLowerCase()
        },
        setFirstName: function (value) {
            Object.defineProperty(user, 'firstName', {writable: true});
            user.firstName = (value);
            Object.defineProperty(user, 'firstName', {writable: false});
        },
        setLastName: function (value) {
            Object.defineProperty(user, 'lastName', {writable: true});
            user.lastName = (value);
            Object.defineProperty(user, 'lastName', {writable: false});
        }
    };
    Object.defineProperty(newUser, 'firstName', {
        value: prompt('Enter first name', 'Maksim'),
        writable: false
    });
    Object.defineProperty(newUser, 'lastName', {
        value: prompt('Enter last name', 'Popov'),
        writable: false
    });
    return newUser
}

let user = createNewUser();
console.log(`Login: ${user.getLogin()}`);