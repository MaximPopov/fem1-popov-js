const display = document.querySelector('.display>input');
const btnsList = document.querySelectorAll('p>input');
const memoryIndicator = document.querySelector('.memory-indicator');
let firstNumber = '';
let operator = '';
let secondNumber = '';
let memory = '';

const calculation = () => {
    switch (operator) {
        case '/':
            return firstNumber / secondNumber;
            break;
        case '*':
            return firstNumber * secondNumber;
            break;
        case '+':
            return +firstNumber + +secondNumber;
            break;
        case '-':
            return firstNumber - secondNumber;
            break;
    }
};

const enterNumber = (key) => {
    if (operator === '') {
        firstNumber += key;
        display.value = firstNumber;
    } else {
        secondNumber += key;
        display.value = secondNumber;
    }
};

const clearAll = () => {
    firstNumber = '';
    operator = '';
    secondNumber = '';
    display.value = '';
};

const getResult = () => {
    if (operator) {
        firstNumber = calculation();
        display.value = firstNumber;
        operator = '';
        secondNumber = '';
    }
};

const memoryAdd = () => {
    memory = +memory + +display.value;
    display.value = memory;
    firstNumber = '';
};

const memorySub = () => {
    memory -= +display.value;
    display.value = memory;
    firstNumber = '';
};

const memoryUse = () => {
    if (operator === '') {
        firstNumber = memory;
        display.value = firstNumber;
    } else {
        secondNumber = memory;
        display.value = secondNumber;
    }
};

const memoryClear = () => {
    memory = '';
    display.value = '';
    firstNumber = '';
    operator = '';
    secondNumber = '';
};

const setOperator = (key) => {
    if (operator === '') {
        operator = key;
    } else {
        firstNumber = calculation();
        display.value = firstNumber;
        operator = key;
        secondNumber = '';
    }
};

const showMemoeyIndicator = ()=> {
    if (memory === '') {
        memoryIndicator.style.display = 'none'
    } else {
        memoryIndicator.style.display = 'block'
    }
};


const keyPressed = (key) => {
    if (!isNaN(+key) || key ==='.') {
        enterNumber(key)
    } else {
        switch (key) {
            case 'C':
                clearAll();
                break;
            case '=':
                getResult();
                break;
            case 'm+':
                memoryAdd();
                break;
            case 'm-':
                memorySub();
                break;
            case 'mrc':
                if (+display.value === +memory) memoryClear();
                else memoryUse();
                break;
            default:
                setOperator(key);
        }
    }
    showMemoeyIndicator();
};

btnsList.forEach((btn) => {
    btn.onclick = (event) => {
        keyPressed(event.target.value);
    }
});

document.onkeyup = (event) => {
    const key = event.key;
    if(!isNaN(+key) || key === '*' || key === '/' || key === '-' || key === '+') {
        keyPressed(key);
    } else if (key === 'Enter') {
        keyPressed('=');
        return false
    } else if (key === 'Escape') {
        keyPressed('C')
    }
};