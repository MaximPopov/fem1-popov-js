let m = Number(prompt("Enter first number", "10"));
let n = Number(prompt("Enter second number", "20"));

while(!((m ^ 0) === m) || !((n ^ 0) === n) || m>=n) {
    alert("This numbers is not correct! Enter numbers again, please.")
    m = Number(prompt("Enter first number", "10"));
    n = Number(prompt("Enter second number", "20"));
}

for (let i=m ; i<n; i++) {
    for (let j=1; j<Math.sqrt(i); j++) {
        if(i%j === 0 && j!==1) {
            break;
        }else if (j === Math.floor(Math.sqrt(i))) {
            console.log(i);
        }
    }

}

/* Alternative version */
/*
for (let i=m ; i<n; i++) {
    for (let j=i-1; j>0; j--) {
        if (j === 1) {
            console.log(i);
        }else if(i%j === 0) {
            break;
        }
    }

}*/
