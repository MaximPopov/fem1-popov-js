function factorial (number) {
    if (number !== 1) {
        return number * factorial(number-1);
    } else {
        return number;
    }
}

let userNumber = +prompt('Enter number', '10');

if (isNaN(userNumber) || userNumber<0) {
    alert('The number is not correct! Enter a number again, please.');
    userNumber = prompt('Enter number', userNumber);
}

alert ('Factorial of ' + userNumber + ' is : ' + factorial(userNumber));