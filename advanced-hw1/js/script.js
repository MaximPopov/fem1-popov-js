/**
 * Класс, объекты которого описывают параметры гамбургера.
 *
 * @constructor
 * @param size        Размер
 * @param stuffing    Начинка
 * @throws {HamburgerException}  При неправильном использовании
 */
function Hamburger(size, stuffing) {
    try {
        if (size === undefined) throw new HamburgerException("no size given");
        if (size.type !== 'size') throw new HamburgerException("invalid size");
        if (stuffing === undefined) throw new HamburgerException("no stuffing given");
        if (stuffing.type !== 'stuffing') throw new HamburgerException("invalid given");
        this._size = size;
        this._stuffing = stuffing;
        this._toppings = []
    } catch (e) {
        console.log('Error: ' + e.message + '!');
    }
}

/* Размеры, виды начинок и добавок */
Hamburger.size = {
    small: {
        type: 'size',
        name: 'small',
        price: 50,
        calories: 20
    },
    large: {
        type: 'size',
        name: 'large',
        price: 100,
        calories: 40
    }
};

Hamburger.stuffing = {
    cheese: {
        type: 'stuffing',
        name: 'cheese',
        price: 10,
        calories: 20
    },
    salad: {
        type: 'stuffing',
        name: 'salad',
        price: 20,
        calories: 5
    },
    potato: {
        type: 'stuffing',
        name: 'potato',
        price: 15,
        calories: 10
    }
};

Hamburger.topping = {
    mayo: {
        type: 'topping',
        name: 'mayo',
        price: 20,
        calories: 5
    },
    spice: {
        type: 'topping',
        name: 'spice',
        price: 15,
        calories: 0
    }
};


/**
 * Добавить добавку к гамбургеру. Можно добавить несколько
 * добавок, при условии, что они разные.
 *
 * @param topping     Тип добавки
 * @throws {HamburgerException}  При неправильном использовании
 */

Hamburger.prototype.addTopping = function (topping) {
    try {
        if (topping === undefined) throw new HamburgerException("no topping given");
        if (topping.type !== 'topping') throw new HamburgerException("invalid topping");
        if (this._toppings.includes(topping)) throw new HamburgerException(`duplicate topping: ${topping.name}`);
        this._toppings.push(topping)
    } catch (e) {
        console.log('Error: ' + e.message + '!');
    }
};

/**
 * Убрать добавку, при условии, что она ранее была
 * добавлена.
 *
 * @param topping   Тип добавки
 * @throws {HamburgerException}  При неправильном использовании
 */

Hamburger.prototype.removeTopping = function (topping) {
    try {
        if (topping === undefined) throw new HamburgerException("no topping given");
        if (topping.type !== 'topping') throw new HamburgerException("invalid topping");
        if (!this._toppings.includes(topping)) throw new HamburgerException(`this burger does not contain topping '${topping.name}'`);
        this._toppings.splice(this._toppings.indexOf(topping), 1)
    } catch (e) {
        console.log('Error: ' + e.message + '!');
    }
};


/**
 * Получить список добавок.
 *
 * @return {Array} Массив добавленных добавок, содержит константы
 *                 Hamburger.TOPPING_*
 */

Hamburger.prototype.getToppings = function () {
    let arr = [];
    this._toppings.forEach((e) => {
        arr.push(e.name)
    });
    return  arr
};


/**
 * Узнать размер гамбургера
 */

Hamburger.prototype.getSize = function () {
    return this._size.name
};

/**
 * Узнать начинку гамбургера
 */

Hamburger.prototype.getStuffing = function () {
    return this._stuffing.name
};


/**
 * Узнать цену гамбургера
 * @return {Number} Цена в тугриках <-- :)
 */

Hamburger.prototype.calculatePrice = function () {
    let price = this._size.price + this._stuffing.price;
    this._toppings.forEach((elem) => {
        price += elem.price
    });
    return price
};


/**
 * Узнать калорийность
 * @return {Number} Калорийность в калориях
 */

Hamburger.prototype.calculateCalories = function () {
    let calories = this._size.calories + this._stuffing.calories;
    this._toppings.forEach((elem) => {
        calories += elem.calories
    });
    return calories
};


/**
 * Представляет информацию об ошибке в ходе работы с гамбургером.
 * Подробности хранятся в свойстве message.
 * @constructor
 */

function HamburgerException (message) {
    this.message = message;
}

/**
 * Тест работоспособности
 * */

// маленький гамбургер с начинкой из сыра
let hamburger = new Hamburger(Hamburger.size.large, Hamburger.stuffing.potato);
// добавка из майонеза
 hamburger.addTopping(Hamburger.topping.mayo);
// спросим сколько там калорий
 console.log("Calories: %f", hamburger.calculateCalories());
// сколько стоит
 console.log("Price: %f", hamburger.calculatePrice());
// я тут передумал и решил добавить еще приправу
 hamburger.addTopping(Hamburger.topping.spice);
 // А сколько теперь стоит?
 console.log("Price with sauce: %f", hamburger.calculatePrice());
// Проверить, большой ли гамбургер?
 console.log("Is hamburger large: %s", hamburger.getSize() === Hamburger.size.large.name); // -> false
// Убрать добавку
 hamburger.removeTopping(Hamburger.topping.mayo);
 console.log("Have %d toppings", hamburger.getToppings().length); // 1
