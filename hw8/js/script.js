const userValue = document.getElementById('userValue');

const div = document.createElement('div');
const span = document.createElement('span');
const btn = document.createElement('button');
btn.innerText = 'x';
btn.onclick = () => {
    div.remove();
    userValue.value = '0';
};
div.appendChild(span);
div.appendChild(btn);

const p = document.createElement('p');
p.innerText = 'Please enter correct price';
p.style.color = 'red';

userValue.onfocus = () => {
    userValue.style.boxShadow = '0 0 2px 1px lightgreen';
};

userValue.onblur = () => {
    if (+userValue.value > 0) {
        userValue.style.boxShadow = '';
        span.innerText = `Текущая цена: ${+userValue.value}`;
        document.body.insertBefore(div, document.body.firstChild);
        p.remove();
        userValue.style.color = 'green';
    } else {
        userValue.style.boxShadow = '0 0 2px 1px red';
        userValue.style.color = '';
        document.body.appendChild(p);
        div.remove();
    }
};
