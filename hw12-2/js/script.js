let index = 0;
let timerValue = 10000;
const imagesList = document.querySelectorAll('.image-to-show');


/*      Подготовка стартового экрана      */

imagesList.forEach((elem)=>{
    elem.style.opacity = '0';
    elem.style.display = 'none';
});
imagesList[index].style.display = 'block';
imagesList[index].style.opacity = '1';


/*      Функция смены слайда (с учетом анимации)    */

const changeSlide = () => {
    imagesList[index].style.opacity = '0';
    setTimeout(() => {
        imagesList[index].style.display = 'none';
        index++;
        if (index === imagesList.length) {
            index = 0;
        }
        imagesList[index].style.display = 'block';
        imagesList[index].style.opacity = '1';
    }, 500);
};


/*      Создание html-элемента для отображения таймера обратного отсчета    */

const timerDisplay = document.createElement('p');
timerDisplay.className = 'timer-display';
document.body.appendChild(timerDisplay);


/*      Функция изменения и отображения значений таймера    */

const startTimer = () => {
    timerValue += -10;
    timerDisplay.innerHTML = `00:${String(timerValue).slice(0, -4) || '0'}${String(timerValue).slice(-4, -3) || '0'}:${String(timerValue).slice(-3)}`;
    if (timerValue === 0) {
        changeSlide();
        timerValue = 10000;
    }
};


/*      Запуск функции изменения значений таймера каждые 10мс   */

let timerId =  setInterval(startTimer, 10);


/*      Создание и вывод кнопки "Прекратить/Возобновить"    */

const stopButton = document.createElement('button');
stopButton.innerHTML = "Прекратить";
stopButton.className = 'btn';
stopButton.onclick = () => {
    switch (stopButton.innerHTML) {
        case "Прекратить":
            clearInterval(timerId);
            stopButton.innerHTML = "Возобновить";
            break;
        case "Возобновить":
            timerId =  setInterval(startTimer, 10);
            stopButton.innerHTML = "Прекратить";
            break;
    }
};
document.body.appendChild(stopButton);