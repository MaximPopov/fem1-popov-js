function calc(firstNumber, secondNumber, operation) {
    switch (operation) {
        case '+':
            return firstNumber + secondNumber;
        case '-':
            return firstNumber - secondNumber;
        case '*':
            return firstNumber * secondNumber;
        case '/':
            return firstNumber / secondNumber;
        default:
            return 'Operation not found!';
    }
}

let firstNumber = +prompt('Enter first number', '10'),
    secondNumber = +prompt('Enter second number', '20'),
    operation = prompt('Enter operation', '+');

while (isNaN(firstNumber) || isNaN(secondNumber)) {
    alert('Numbers is not correct! Enter numbers again, please.');
    firstNumber = +prompt('Enter first number', firstNumber);
    secondNumber = +prompt('Enter second number', secondNumber);
}

console.log(calc(firstNumber, secondNumber, operation));;