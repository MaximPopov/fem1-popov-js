function createList(arr) {
    document.body.appendChild(document.createElement('ul'));
    arr.map(function(elem) {
        let newTag = document.createElement('li');
        newTag.innerHTML = `${elem}`;
        document.querySelector('body>ul').appendChild(newTag);
    });
}

/*
function createList(arr) {
    document.body.appendChild(document.createElement('ul'));
    let newTag;
    for (let key in arr) {
        newTag = document.createElement('li');
        newTag.innerHTML = `${arr[key]}`;
        document.querySelector('body>ul').appendChild(newTag);
    }
}*/

createList(['hello', 'world', 'Kiev', 'Kharkiv', 'Odessa', 'Lviv']);